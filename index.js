// Hide/show menu mobile
const toggleBtn = document.querySelector(".header-toggle");
const menuMobile = document.querySelector(".menu-mobile-wrapper");

const toggleMenuMobile = () => {
  if (menuMobile.className.includes("active")) {
    menuMobile.classList.remove("active");
  }
  else {
    menuMobile.classList.add("active");
  }
}
toggleBtn.onclick = () => toggleMenuMobile();

const sliderItems = document.querySelectorAll(".slider-item");
const sliderPrev = document.querySelector(".slider__prev");
const sliderNext = document.querySelector(".slider__next");

// Header slider
const handlerSlider = function () {
  let id = 0;
  const prevSlide = function () {
    if (id === 0) {
      id = sliderItems.length - 1;
    } else {
      id -= 1;
    }
    sliderItems.forEach((sliderItem) => {
      if (sliderItem.className.includes("active")) {
        sliderItem.classList.remove("active");
      }
    })
    sliderItems[id].classList.add("active")
  }

  const nextSlide = function () {
    if (id === sliderItems.length - 1) {
      id = 0;
    } else {
      id += 1;
    }
    sliderItems.forEach((sliderItem) => {
      if (sliderItem.className.includes("active")) {
        sliderItem.classList.remove("active");
      }
    })
    sliderItems[id].classList.add("active")
  }

  sliderPrev.addEventListener("click", function () {
    prevSlide();
  });
  sliderNext.addEventListener("click", function () {
    nextSlide();
  });
  setInterval(() => {
    nextSlide();
  }, 5000);
}

const productSliderWrapper = document.querySelector(".product-list-wrapper");
const productSlider = document.querySelector(".product-list.active");
const productItems = productSlider.querySelectorAll(".product-item-wrapper");
const productItem = document.querySelector(".product-item-wrapper");
const productPrev = document.querySelector(".product-list-wrapper__prev");
const productNext = document.querySelector(".product-list-wrapper__next");

const productTabs = document.querySelectorAll(".product-tabs-item");
const productContent = document.querySelectorAll(".product-list");

const handleProductTab = function () {
  productTabs.forEach((productTab, index) => {
    productTab.addEventListener("click", function () {
      productTabs.forEach((productTab) => {
        if (productTab.className.includes("active")) {
          productTab.classList.remove("active");
        }
      })
      productContent.forEach((productContentItem) => {
        if (productContentItem.className.includes("active")) {
          productContentItem.classList.remove("active");
        }
      })
      productTab.classList.add("active");
      productContent[index].classList.add("active");
    })
  })
}

const sliderItem = function (sliderWrapper, slider, items, item, prev, next) {
  let translateValue = 0;
  let itemWidth = item.offsetWidth;
  let sliderWrapperWidth = sliderWrapper.offsetWidth;
  let itemsLength = items.length;
  window.addEventListener("resize", function () {
    translateValue = 0;
    slider.style.transform = `translateX(${translateValue}px)`;
    itemWidth = item.offsetWidth;
    sliderWrapperWidth = sliderWrapper.offsetWidth;
  });

  const prevSlide = function () {
    if (translateValue >= 0) {
      translateValue = -(itemWidth * itemsLength - sliderWrapperWidth);
    }
    else {
      translateValue += itemWidth;
    }
    slider.style.transform = `translateX(${translateValue}px)`;
  };

  const nextSlide = function () {
    if (translateValue <= -(itemWidth * itemsLength - sliderWrapperWidth)) {
      translateValue = 0;
    }
    else {
      translateValue -= itemWidth;
    }
    slider.style.transform = `translateX(${translateValue}px)`;
  };

  prev.addEventListener("click", function () {
    prevSlide();
  });

  next.addEventListener("click", function () {
    nextSlide();
  });
}

const postSliderWrapper = document.querySelector(".post-list-wrapper");
const postSlider = document.querySelector(".post-list");
const postItems = postSlider.querySelectorAll(".post-item-wrapper");
const postItem = document.querySelector(".post-item-wrapper");
const postPrev = document.querySelector(".post-list-wrapper__prev");
const postNext = document.querySelector(".post-list-wrapper__next");

window.onload = function () {
  handlerSlider();
  handleProductTab();
  sliderItem(productSliderWrapper, productSlider, productItems, productItem, productPrev, productNext);
  sliderItem(postSliderWrapper, postSlider, postItems, postItem, postPrev, postNext);
};
